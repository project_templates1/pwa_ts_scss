const gulp = require('gulp');
const browserSync = require('browser-sync');
const gulpSass = require('gulp-sass')(require('sass'));
const gulpTS = require('gulp-typescript');


//  INFO: Configuration object to store the compile and server details

const Config =  {
    bsSetup: {
        reloadList: [
            'app/**/*.html',
            'app/dist/css/**/*.css',
            'app/dist/javascript/**/*.js'
        ],
        init: {
            port: 3000,
            server: { baseDir: './app' },
            files: [
                'app/dist/**/*',
                'app/assets/**/*'
            ]
        }
    },

    scssSetup: {
        input: 'src/scss/**/*.scss',
        output: 'app/dist/css',
        compilerOptions: {
            outputStyle: 'compressed'
        }
    }
};


//  INFO: Functions to compile the files and run the server

const bs = () =>
    browserSync
        .init(Config.bsSetup.init);

const sass = () =>
    gulp.src(Config.scssSetup.input)
        .pipe(gulpSass(Config.scssSetup.compilerOptions)
            .on('error', gulpSass.logError))
        .pipe(gulp.dest(Config.scssSetup.output));

const dev = () => {
    bs();
    Config.bsSetup.reloadList.forEach(file => {
        gulp.watch(file)
            .on('change', browserSync.reload);
    });
    gulp.watch(Config.scssSetup.input, sass);
};


//  INFO: Exported gulp commands

exports.bs = bs;
exports.sass = sass;
exports.dev = dev;
